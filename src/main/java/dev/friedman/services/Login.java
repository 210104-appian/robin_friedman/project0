package dev.friedman.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import dev.friedman.daos.BankDaoImpl;
import dev.friedman.util.ScannerUtil;

public class Login {
	private String email = null;    // User input
	private String password = null; // "
	private String name = null; // Queried upon successful email/password match
	private UserPrivilege accountType = null; // "
	
	// This became an illegible mess. Maybe a fresher Robin will make it sightly once more.
	private boolean loggingIn = false;
	public boolean success = false;
	public boolean quitting = false;

	public Login(Connection connection) {
		do {
			loggingIn = true;
			// Initialize as null
			String getPassword = "SELECT PASSWORD FROM USERLIST WHERE EMAIL = ?";
			PreparedStatement askPassword = null;
			ResultSet fetchedPassword = null;

			do {
				
				// Receive User input
				System.out.println("Welcome to the Bank.net app!\nEnter email: "); // try: beepkun@yahoo.com OR try: empl1@bank.net
				email = ScannerUtil.in.nextLine();				
				if (email.equals("0") || email.equalsIgnoreCase("Q")) {
					break;
				} 
				System.out.println("Enter password: "); // try: mediumC04r OR try: kloiflish
				password = ScannerUtil.in.nextLine();

				// Try login with input
				try {
					// Prepare and execute SQL Statement
					askPassword = connection.prepareStatement(getPassword);
					askPassword.setString(1, email);
					fetchedPassword = askPassword.executeQuery();

					// If email matched a record, ResultSet will contain its password
					if (fetchedPassword.next()) {
						String returnedPW = fetchedPassword.getString("PASSWORD");	
						// If the password enter matches the fetchedPassword for that user
						if (password.equals(returnedPW)) {
							System.out.println("Login successful!");
							success = true;
							loggingIn = false;
							// Continue by querying for the UserPrivilege and Name
							accountType = queryType(connection, email);
							name = queryName(connection, email);
						} else {
							System.out.println("Password incorrect.");
						}
					}	else {
						System.out.println("No user found for that email.");
					}
				} catch (SQLException e) {
					System.out.println("Exception during Login.");
				}
			} while (success == false); 

			if (success == false) { // Remains false when breaking from the block above.
				System.out.println("Do you really want to quit? [Q]uit ");
				String answer = ScannerUtil.in.nextLine();
				if (answer.equalsIgnoreCase("Q")) {
					loggingIn = false;
					quitting = true;
				}
			}
		} while (loggingIn);
	}
	
	public String queryName(Connection connection, String email) throws SQLException {
		String getName = "SELECT NAME FROM USERLIST WHERE EMAIL = ?";

		PreparedStatement askName = connection.prepareStatement(getName);
		askName.setString(1, email);
		ResultSet fetchedName = askName.executeQuery();

		if (fetchedName.next()) {
			name = fetchedName.getString("NAME");
			return name;
		}	else {
			System.out.println("No data returned.");
		}
		
		return null;
	}
	
	public String getName(Connection connection) throws SQLException {
		if (name != null) {
			return this.name;
		} else {
			if (this.email != null) {
				return queryName(connection, email);				
			} else {
				System.out.println("Error getting Name: email not specified.");
				return null;
			}
		}
	}

	public static UserPrivilege queryType(Connection connection, String email) throws SQLException {
		String getAccountType = "SELECT PRIVILEGE FROM USERLIST WHERE EMAIL = ?";
		String accountType = null;

		PreparedStatement askAccountType = connection.prepareStatement(getAccountType);
		askAccountType.setString(1, email);
		ResultSet fetchedAccountType = askAccountType.executeQuery();

		if (fetchedAccountType.next()) {
			accountType = fetchedAccountType.getString("PRIVILEGE");
		}	else {
			System.out.println("No data returned.");
		}

		switch(accountType) {
		case "CUS": 
			return UserPrivilege.CUS;
		case "EMP":
			return UserPrivilege.EMP;
		case "MAN":
			return UserPrivilege.MAN;
		}
		return null;
	}

	public UserPrivilege getType(Connection connection) throws SQLException {
		if (accountType != null) {
			return this.accountType;
		} else {
			if (this.email != null) {
				return queryType(connection, email);				
			} else {
				System.out.println("Error getting UserPrivilege: email not specified.");
				return null;
			}
		}
	}

}