package dev.friedman.services;

import java.sql.SQLException;

public interface View {

	boolean viewing = true;
	
	void mainMenu() throws SQLException;
	
}
