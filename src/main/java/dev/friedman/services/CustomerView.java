package dev.friedman.services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import dev.friedman.daos.CustomerDaoImpl;
import dev.friedman.models.Account;
import dev.friedman.models.Transaction;
import dev.friedman.models.TransactionType;
import dev.friedman.util.ChoiceUtil;
import dev.friedman.util.ScannerUtil;

public class CustomerView implements View {
	String name;
	String input;
	boolean viewing;

	Scanner in = null;
	CustomerDaoImpl dao = null;
	ArrayList<Account> accounts = null;
	ArrayList<Transaction> allTransactions = null;

	public CustomerView() throws SQLException {
		System.out.print("Welcome, Customer!");
		dao = new CustomerDaoImpl();
	}
	public CustomerView(String name) throws SQLException {
		System.out.print("Welcome, " + name + "! ");
		this.name = name;
		dao = new CustomerDaoImpl();
	}

	@Override
	public void mainMenu() throws SQLException {
		// Initialize menu navigation
		input = null;
		int choice = -1;
		Account accountSelected = null;

		System.out.println("What would you like to do?");
		System.out.println("[1] Check account balance\n"
				+ "[2] Deposit money to account\n"
				+ "[3] Withdraw money from account\n"
				+ "[4] Apply for NEW account\n"
				+ "[0] Log out");
		choice = ChoiceUtil.getIntChoice(1, 4, true);

		if (choice >= 1 && choice <=3) {
			// Determine which account we're looking at
			int accountChoice;
			// Find customer's bank accounts
			accounts = dao.getAccountsByCustomer(name);

			if (accounts != null) {
				do {			
					if (accounts.size() > 1) {
						System.out.println("For which account?");
						for (int i = 0; i < accounts.size(); i++) {
							System.out.println("[" + (i+1) + "] Account No. " + accounts.get(i).getAccountNO() + " " + accounts.get(i).getAccountType());
						}
						accountChoice = ChoiceUtil.getIntChoice(1, accounts.size(), true);
						if (accountChoice == 0) {
							System.out.println("Returning to Main Menu.");
							mainMenu();
						} else {
							accountSelected = accounts.get(accountChoice - 1); // Reset choice to null in the case of a non-integer entry.
						} 
					} else {
						accountSelected = accounts.get(0); 
					}
				} while (accountSelected == null);
			} else {
				System.out.println("No accounts found.");
				mainMenu();
			}
		}

		switch(choice) {
		case 1: 
			System.out.println("Balance of Account #" + accountSelected.getAccountNO() + ": " + accountSelected.getBalance());
			mainMenu();
		case 2: 
			customerDeposit(accountSelected.getAccountNO());
			mainMenu();
			break;
		case 3: 
			break;
		case 4:
			break;
		case 0:
			System.out.println("Logging out.");
			break;
		}
	}

	private void customerDeposit(int no) throws SQLException {
		System.out.println("How much would you like to deposit?");
		double amount = -0.01;
		input = ScannerUtil.in.nextLine();
		do {
			try {
				amount = Double.valueOf(input);
			} catch (NumberFormatException e) {
				System.out.println("Enter the amount, including cents. For example, '100.00'");
				amount = 0.0;
			}
		} while (amount == -0.01);
		
		Transaction deposit = new Transaction();
		deposit.setTransType(TransactionType.DEPOS);
		deposit.setAmount(amount); // Amount set to 0.0 if negative
		deposit.setSource(0);
		deposit.setTarget(no);
		//deposit.setDate(); // We'll handle this on the DB side
		
		if (deposit.getAmount() > 0.0) { // If there's money to move...
			double newBalance = dao.depositMoney(deposit);
			System.out.println("New balance in Account #" + no + ": " + newBalance);
		} else {
			return;
		}
	}

}
