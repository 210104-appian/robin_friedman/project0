package dev.friedman.services;

import dev.friedman.util.ChoiceUtil;
import dev.friedman.util.ScannerUtil;

import java.sql.SQLException;
import java.util.ArrayList;

import dev.friedman.daos.BankDaoImpl;
import dev.friedman.models.Account;
import dev.friedman.models.Transaction;

public class EmployeeView implements View {
	private String input;
	private ChoiceUtil choices;
	public boolean viewing;

	private BankDaoImpl dao = null;
	private ArrayList<Account> pending = null;
	private ArrayList<Account> accounts = null;
	private ArrayList<Transaction> transactions = null;

	public EmployeeView() throws SQLException {
		// Default is a general Employee
		System.out.print("Welcome, Employee! ");
		dao = new BankDaoImpl();		
	}
	public EmployeeView(String privilege) throws SQLException {
		// This is current implementation for acknowledging the Manager, 
		//...without actually giving them special permissions at this time.
		System.out.print("Welcome, " + privilege + "! ");
		dao = new BankDaoImpl();		
	}

	public void mainMenu() throws SQLException {
		System.out.println("What would you like to do?");
		System.out.println("[1] View pending customer accounts\n"
				+ "[2] Find customer accounts by name\n"
				+ "[3] View customer transactions by name\n"
				+ "[0] Log out");
		int choice = choices.getIntChoice(1, 3, true);

		switch(choice) {
		case 1: 
			pending = dao.getAllPendingAccounts();
			showPendingAccounts(pending); // In a larger database, would need to control how many accounts are shown at once.
			break;
		case 2: 
			System.out.println("Please enter the full name of the customer.");
			String customerNameA = ScannerUtil.in.nextLine();
			accounts = dao.getAccountsByCustomer(customerNameA);
			showCustomerAccounts(accounts);
			break;
		case 3: 
			System.out.println("Please enter the full name of the customer.");
			String customerNameT = ScannerUtil.in.nextLine();
			transactions = dao.getTransactionsByCustomer(customerNameT);
			showCustomerTransactions(transactions);
			break;
		case 0:
			System.out.println("Logging out.");
			viewing = false;
			break;
		}
	}

	private void showCustomerTransactions(ArrayList<Transaction> transactions) throws SQLException {
		if (transactions != null) {
			System.out.println("Here are all account transactions for this customer:");
			for (Transaction transaction : transactions) {
				System.out.println(transaction);
			}
		} else {
			System.out.println("No transactions for this customer name.");
		}
		mainMenu();
	}

	private void showCustomerAccounts(ArrayList<Account> accounts) throws SQLException {
		if (accounts != null) {
			System.out.println("Here are all accounts for this customer:");
			System.out.println(accounts);

			// Check if there are any accounts for this customer pending.
			ArrayList<Account> customerPendingAccounts = new ArrayList<Account>();
			for (Account account : accounts) {
				if (!account.getApproval()) {
					customerPendingAccounts.add(account);
				}
			}
			// If there are any pending accounts, pass them to the iterative approval method.
			if (customerPendingAccounts.size() > 0) {
				System.out.print("Approve accounts for this customer? [Y/N] ");
				input = ChoiceUtil.getYNChoice();
				switch (input) {
				case "Y": 
					iterateAccountsForApproval(customerPendingAccounts);
					break;
				case "N":
					break;
				}
			} 
		}
		// Return to Employee Main Menu
		mainMenu();
	}

	private void iterateAccountsForApproval(ArrayList<Account> accounts) throws SQLException {
		// For each Account in the given ArrayList, ask about approval.
		for (Account account : accounts) {
			int no = account.getAccountNO();
			System.out.print(account + "\nApprove? [Y/N] ");
			input = ChoiceUtil.getYNChoice();
			switch(input) {
			case "Y": 
				dao.approveAccount(no); // dao's method has its own success message.
				break;
			case "N":
				System.out.println("Account " + no + " remains not approved.");
				break;
			}
		}
	}

	private void showPendingAccounts(ArrayList<Account> pending) throws SQLException {
		if (pending != null) {
			System.out.println("Here are all pending customer accounts:");
			for (Account account : pending) {
				System.out.println(account);
			}
			System.out.println("Would you like to approve an account?\n"
					+ "[YES] Approve accounts 1 at a time.\n"
					+ "[ALL] Approve all pending accounts.\n"
					+ "[NO] Return to Main Menu.");
			do {
				input = ScannerUtil.in.nextLine().toUpperCase();
			} while (!input.equals("YES") && !input.equals("ALL") && !input.equals("NO"));

			switch(input) {
			case "YES":
				// Calls class method to iterate through pending Account list.
				iterateAccountsForApproval(pending);
				break;
			case "ALL":
				// The dao has its own method for approving all accounts.
				dao.approveAllAccounts();
			case "NO":
				break;
			}
		} else {
			System.out.println("No pending accounts. Returning to Main Menu."); 
		}
		mainMenu();
	}

}
