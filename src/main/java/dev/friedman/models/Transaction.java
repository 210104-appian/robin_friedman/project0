package dev.friedman.models;

public class Transaction {

	private TransactionType transType;
	private double amount;
	private int source;
	private int target;
	private String date; // "YYYY-MM-DD"
	
	public TransactionType getTransType() {
		return transType; // Five letter abbreviations: WITHD, DEPOS, TRANS
	}

	public void setTransType(TransactionType transType) {
		this.transType = transType; // Dao will need to determine this based on values of Source and Target in db
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		if (amount > 0.0) {
			this.amount = amount;
			return;
		} else {
			this.amount = 0.0;
		}
	}

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}

	public int getTarget() {
		return target;
	}

	public void setTarget(int target) {
		this.target = target;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		int source = this.source;
		int target = this.target;
		return transType + " of amount " + amount + " from " + (source > 0? source : "pocket") + " to " + 
				(target > 0? target : "pocket") + " on date/time: " + date;
	}

}
