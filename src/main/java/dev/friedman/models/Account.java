package dev.friedman.models;

public class Account {
	private int no;
	private int ownerID;
	private AccountType kind;
	private double balance;
	private boolean approved;
	
	public int getAccountNO() {
		return this.no;
	}
	
	public void setAccountNO(int no) {
		this.no = no;
	}
	
	public int getOwnerID() {
		return this.ownerID;
	}
	
	public void setOwnerID(int id) {
		this.ownerID = id;
	}
	
	public AccountType getAccountType() {
		return this.kind;
	}
	
	public void setAccountType(String kind) {
		this.kind = AccountType.valueOf(kind);
	}
	
	public double getBalance() {
		return this.balance;
	}
	
	public void setBalance(double balance) { 
		this.balance = balance;
	}
	
	public boolean getApproval() {
		return this.approved;
	}
	
	public void setApproval(String approved) {
		if (approved.equals("Y")) {
			this.approved = true;
		} else {
			this.approved = false;
		}
	}
	
	@Override
	public String toString() {
		return "Account #" + no + ", owned by Customer ID " + ownerID + ", is a " + kind + " account with a balance of " + balance + ". It is approved: "	+ approved;
	}

}
