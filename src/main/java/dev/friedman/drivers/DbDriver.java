package dev.friedman.drivers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import dev.friedman.util.ConnectionUtil;
import dev.friedman.services.*;

public class DbDriver {

	public static Logger log = Logger.getRootLogger();

	public static void main(String[] args) {
		Connection connection = null;
		UserPrivilege accountType = null;
		String accountHolder = null;
		View view = null;

		do {
			try {
				// Try connecting to the database
				connection = ConnectionUtil.getConnection();

				// Use the connection to create a new Login session
				Login session = new Login(connection);
				if (!session.quitting) {
					accountType = session.getType(connection); // UserPrivilege determines subsequent View
					accountHolder = session.getName(connection); // Customers are greeted with their name
					switch (accountType) {
					case CUS:
						view = new CustomerView(accountHolder);
						break;
					case EMP: 
						view = new EmployeeView("Employee");
						break;
					case MAN: 
						// create manager view
						view = new EmployeeView("Manager");
						break;
					}
					view.mainMenu();
				} else {
					System.out.println("Quitting.");
					break; // Without a view, viewing will be false too
				}
			} catch (SQLException e) {
				log.error(e);
			} finally {
				if (connection != null ) { // close Connection
					try {
						connection.close();
					} catch (SQLException e) { }
				}
			}
		} while (view.viewing);
	}

}