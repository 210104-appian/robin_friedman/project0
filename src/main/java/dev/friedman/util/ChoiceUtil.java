package dev.friedman.util;

public class ChoiceUtil {
	private static boolean zeroEscapes;
	private static String input;
	private static int choice;

	public static String getYNChoice() {
		do {
			input = ScannerUtil.in.nextLine().toUpperCase();
		} while (!input.equals("Y") && !input.equals("N"));
		return input;
	}
	
	public static int getIntChoice(int min, int max, boolean zeroEscapes) {
		choice = -1; // Initialize out of range.

		do {
			input = ScannerUtil.in.nextLine();
			try {
				choice = Integer.valueOf(input);
			} catch (NumberFormatException e) {
				System.out.println("Enter the number of your choice (between " + min + " and " + max + "). " + (zeroEscapes? "Or enter 0 to go back." : ""));
				choice = -1; // Reset choice to -1 in the case of a non-integer entry.
			}
		} while (!choiceIsInRange(choice, min, max, zeroEscapes));
		
		return choice;
	}

	public static boolean choiceIsInRange(int choice, int min, int max, boolean zeroEscapes) {
		if (zeroEscapes && choice == 0) {
			return true;
		} else if (choice < min) {
			System.out.println("Choose a number between " + min + " and " + max + ".");
			return false;
		} else if (choice > max) {
			System.out.println("Choose a number between " + min + " and " + max + ".");
			return false;
		} else {
			return true;
		}
	}
}
