package dev.friedman.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {
	private static Connection connection;
	
	public static Connection getConnection() throws SQLException {
		// Access credentials to database are stored in Environment Variables.
		String url = System.getenv("DB_URL");
		String un = System.getenv("DB_USER");
		String pw = System.getenv("DB_PW");
		
		if (connection == null || connection.isClosed()) {
			connection = DriverManager.getConnection(url, un, pw);
		}
		return connection;
	}
}
