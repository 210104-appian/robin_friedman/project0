package dev.friedman.daos;

import java.sql.SQLException;
import java.util.ArrayList;

import dev.friedman.models.Account;
import dev.friedman.models.Transaction;

public interface BankDao {	
	public ArrayList<Account> getAllPendingAccounts() throws SQLException; // Get list of ALL pending accounts
	public boolean approveAccount(int accountNO) throws SQLException; // Update approved to 'Y'
	public int approveAllAccounts() throws SQLException; // Update ALL approved to 'Y'
	public ArrayList<Account> getAccountsByCustomer(String name) throws SQLException; // Get all accounts owned by a specific Customer
	public ArrayList<Transaction> getTransactionsByCustomer(String name) throws SQLException;	// Get all transactions by customer name
	
	/* (future)
	 * USER
	 * - create new user account (default 'CUS')
	 * - change password
	 * 
	 * - MANAGER class
	 * - - view employee list
	 * - - create new employee account
	 * - - remove employee account
	 * - - view log of all actions taken in the app/db
	 */
}
