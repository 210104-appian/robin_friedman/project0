package dev.friedman.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dev.friedman.models.Account;
import dev.friedman.models.Transaction;
import dev.friedman.util.ConnectionUtil;

public class CustomerDaoImpl implements CustomerDao {
	Connection db = null;
	
	public CustomerDaoImpl() throws SQLException {
		db = ConnectionUtil.getConnection();
	}

	@Override
	public ArrayList<Account> getAccountsByCustomer(String name) throws SQLException {
		System.out.println("Searching for your account(s)...");
		String query = "SELECT * FROM ACCOUNTS_OF_CUSTOMER WHERE NAME = ?";
		PreparedStatement getAccounts = db.prepareStatement(query);
		getAccounts.setString(1, name);
		ResultSet accounts = getAccounts.executeQuery();
		ArrayList<Account> yourAccounts = new ArrayList<Account>();

		while (accounts.next()) {
			Account account = new Account();
			account.setAccountNO(accounts.getInt("NO"));
			account.setOwnerID(accounts.getInt("ID")); // this shouldn't be needed
			account.setApproval(accounts.getString("APPROVED"));
			account.setAccountType(accounts.getString("KIND"));
			account.setBalance(accounts.getDouble("BALANCE"));
			yourAccounts.add(account);
		}
		if (yourAccounts.size() != 0) {
			return yourAccounts;
		} else {
			System.out.println("No accounts found.");
			return null;
		}	
	}
	
	@Override
	public Account applyForNewBankAccount() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double depositMoney(Transaction deposit) throws SQLException {
		System.out.println("Depositing...");
		String insert = "INSERT INTO TRANSACTIONS (SOURCE_NO, TARGET_NO, AMOUNT, T_DATE) VALUES (?, ?, ?, (SELECT CURRENT_DATE FROM DUAL));";
		PreparedStatement insertTrans = db.prepareStatement(insert);
		insertTrans.setInt(1, deposit.getSource());
		insertTrans.setInt(2, deposit.getTarget());
		insertTrans.setDouble(3, deposit.getAmount());
		boolean success = insertTrans.execute();
		if (success) {
			String query = "SELECT UNIQUE BALANCE FROM ACCOUNTLIST WHERE ACCOUNT_NO = ?";
			PreparedStatement queryBalance = db.prepareStatement(query);
			queryBalance.setInt(1, deposit.getTarget());
			ResultSet newBalance = queryBalance.executeQuery();
			if (newBalance.next()) {
				return newBalance.getDouble("BALANCE");
			}
		}

		return 0.0;
	}

	@Override
	public double withdrawMoney(Transaction withdrawal) throws SQLException {

		return 0.0;
	}

}
