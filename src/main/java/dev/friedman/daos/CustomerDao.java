package dev.friedman.daos;

import java.sql.SQLException;
import java.util.ArrayList;

import dev.friedman.models.Account;
import dev.friedman.models.Transaction;

public interface CustomerDao {
	public Account applyForNewBankAccount() throws SQLException; // Not sure about this return type
	public double depositMoney(Transaction deposit) throws SQLException; // Return updated Balance
	public double withdrawMoney(Transaction withdrawal) throws SQLException; // Return updated Balance
	ArrayList<Account> getAccountsByCustomer(String name) throws SQLException; // Use name to find customer's accounts
	
	/* (future)
 	 * CUSTOMER
 	 * - transfer money
	 */
}
