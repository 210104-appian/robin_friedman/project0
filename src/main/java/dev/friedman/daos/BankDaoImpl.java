package dev.friedman.daos;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import dev.friedman.models.Account;
import dev.friedman.models.Transaction;
import dev.friedman.models.TransactionType;
import dev.friedman.util.ConnectionUtil;

public class BankDaoImpl implements BankDao {
	Connection db = null; 

	public BankDaoImpl () throws SQLException {
		db = ConnectionUtil.getConnection();
	}

	@Override
	public ArrayList<Account> getAllPendingAccounts() throws SQLException {
		System.out.println("Getting all pending accounts...");
		String query = "SELECT * FROM ACCOUNTLIST WHERE APPROVED = 'N'";
		Statement pendingStatement = db.createStatement();
		ResultSet pending = pendingStatement.executeQuery(query);
		ArrayList<Account> pendingAccounts = new ArrayList<Account>();

		while (pending.next()) {
			Account account = new Account();
			account.setAccountNO(pending.getInt("ACCOUNT_NO"));
			account.setOwnerID(pending.getInt("OWNER_ID"));
			account.setAccountType(pending.getString("KIND"));
			account.setBalance(pending.getDouble("BALANCE"));
			account.setApproval(pending.getString("APPROVED"));
			pendingAccounts.add(account);
		}
		if (pendingAccounts.size() != 0) {
			return pendingAccounts;
		} else {
			return null;
		}
	}

	@Override
	public boolean approveAccount(int accountNO) throws SQLException {
		System.out.println("Approving Account " + accountNO);
		String update = "UPDATE ACCOUNTLIST SET APPROVED = 'Y' WHERE ACCOUNT_NO = ?";
		PreparedStatement approveAccount = db.prepareStatement(update);
		approveAccount.setInt(1, accountNO);
		int success = approveAccount.executeUpdate();
		if (success != 0) {
			System.out.println("Account " + accountNO + " successfully approved.");
			return true;
		}
		System.out.println("Account " + accountNO + " was not approved.");
		return false;
	}

	@Override
	public int approveAllAccounts() throws SQLException {
		System.out.println("Approving all pending accounts...");
		//		String update = "UPDATE ACCOUNTLIST SET APPROVED = 'Y' WHERE OWNER_ID = (SELECT * FROM GET_ALL_APPROVED_USERS)";
		String update = "UPDATE ACCOUNTLIST SET APPROVED = 'Y' WHERE APPROVED = 'N'";
		Statement approveAll = db.createStatement();
		int count = approveAll.executeUpdate(update);

		System.out.println(count + " accounts successfully approved.");
		return count;
	}

	@Override
	public ArrayList<Account> getAccountsByCustomer(String name) throws SQLException {
		System.out.println("Searching for customer " + name + "...");
		String query = "SELECT * FROM ACCOUNTS_OF_CUSTOMER WHERE NAME = ?";
		PreparedStatement getAccounts = db.prepareStatement(query);
		getAccounts.setString(1, name);
		ResultSet accounts = getAccounts.executeQuery();
		ArrayList<Account> customerAccounts = new ArrayList<Account>();

		while (accounts.next()) {
			Account account = new Account();
			account.setAccountNO(accounts.getInt("NO"));
			account.setOwnerID(accounts.getInt("ID")); // this shouldn't be needed
			account.setApproval(accounts.getString("APPROVED"));
			account.setAccountType(accounts.getString("KIND"));
			account.setBalance(accounts.getDouble("BALANCE"));
			customerAccounts.add(account);
		}
		if (customerAccounts.size() != 0) {
			return customerAccounts;
		} else {
			System.out.println("No accounts under this name.");
			return null;
		}	
	}

	@Override
	public ArrayList<Transaction> getTransactionsByCustomer(String name) throws SQLException {
		System.out.println("Retrieving transactions of customer " + name + "...");
		String query = "SELECT * FROM TRANSACTIONS_OF_ACCOUNTS_BY_OWNER_NAME WHERE NAME = ?	ORDER BY T_DATE";
		PreparedStatement getTransactions = db.prepareStatement(query);
		getTransactions.setString(1,  name);
		ResultSet transactions = getTransactions.executeQuery();
		ArrayList<Transaction> customerTransactions = new ArrayList<Transaction>();

		while (transactions.next()) {
			Transaction transaction = new Transaction();
			transaction.setDate(transactions.getString("T_DATE"));
			transaction.setSource(transactions.getInt("SOURCE"));
			transaction.setTarget(transactions.getInt("TARGET"));
			transaction.setAmount(transactions.getDouble("AMOUNT"));
			
			// 0 is the number of someone's pockets
			if (transaction.getSource() != 0 && transaction.getTarget() != 0) {
				transaction.setTransType(TransactionType.TRANS);
			} else if (transaction.getSource() == 0 && transaction.getTarget() == 0) {
				System.out.println("[You shouldn't be here.]");
				transaction.setTransType(null); // Transactions shouldn't exist if money isn't being moved to or from an account
			}
			else if (transaction.getSource() == 0) {
				transaction.setTransType(TransactionType.DEPOS);
			} else if (transaction.getTarget() == 0) {
				transaction.setTransType(TransactionType.WITHD);			
			}
			customerTransactions.add(transaction);
		}
		return customerTransactions;
	}

}
